package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var welcomeMessage: TextView
    private lateinit var helloMessage: TextView
    private lateinit var btnCambiarTexto: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        welcomeMessage = findViewById(R.id.txtBienvenida)
        helloMessage = findViewById(R.id.txtSaludo)
        btnCambiarTexto = findViewById(R.id.txtBoton)

        btnCambiarTexto.setOnClickListener{cambiarTexto()}

    }


    private fun cambiarTexto(){
        welcomeMessage.text = "Bienvenido Multiverso"
        helloMessage.text = "Hola Multiverso"

    }
}
