interface tecnologia{
    fun entretener(): String
    fun comunicar(): String
    fun investigar(): String
}

class Computadora : tecnologia{
    override fun entretener(): String {
        return "¡¡¡La computadora esta entreteniendo!!!"
    }

    override fun comunicar(): String {
        return "¡¡¡La computadora esta comunicándose!!!"
    }

    override fun investigar(): String {
        return "¡¡¡La computadora esta investigando!!!"
    }
}

class Celular : tecnologia{
    override fun entretener(): String {
        return "¡¡¡El celular esta entreteniendo!!!"
    }

    override fun comunicar(): String {
        return "¡¡¡El celular esta comunicándose!!!"
    }

    override fun investigar(): String {
        return "¡¡¡El celular esta investigando!!!"
    }

}

enum class Aparatos_tecnologicos{
    Huawei, Dell, Iphone, Samsung, Mac
}

fun clasificacion(at: Aparatos_tecnologicos): tecnologia{
    when(at){
        Aparatos_tecnologicos.Dell, Aparatos_tecnologicos.Mac -> return Celular()
        Aparatos_tecnologicos.Huawei, Aparatos_tecnologicos.Iphone, Aparatos_tecnologicos.Samsung -> return Computadora()
    }
}

fun main(args: Array<String>){
    println(clasificacion(Aparatos_tecnologicos.Mac).entretener())
    println(clasificacion(Aparatos_tecnologicos.Huawei).comunicar())
}